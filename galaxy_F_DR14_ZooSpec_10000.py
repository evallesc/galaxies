

     # IN GOOGLE COLAB. TO MOUNT DRIVE

# !apt-get install -y -qq software-properties-common python-software-properties module-init-tools
# !add-apt-repository -y ppa:alessandro-strada/ppa 2>&1 > /dev/null
# !apt-get update -qq 2>&1 > /dev/null
# !apt-get -y install -qq google-drive-ocamlfuse fuse
# from google.colab import auth
# auth.authenticate_user()
# from oauth2client.client import GoogleCredentials
# creds = GoogleCredentials.get_application_default()
# import getpass
# !google-drive-ocamlfuse -headless -id={creds.client_id} -secret={creds.client_secret} < /dev/null 2>&1 | grep URL
# vcode = getpass.getpass()
# !echo {vcode} | google-drive-ocamlfuse -headless -id={creds.client_id} -secret={creds.client_secret}
 
#!mkdir -p drive
#!google-drive-ocamlfuse drive
#!pip install -q keras

     # for uploading the data from drive to colab (already mounted)
#!cp drive/Practiques_data/F_DR14_ZooSpec_10000.zip .
#!unzip F_DR14_ZooSpec_10000.zip 
#!cp drive/Practiques_data/DR14_ZooSpec.csv .
#!cp drive/Practiques_data/F_DR14_ZooSpec_10000_classified.csv .
#!cp drive/Practiques_data/64x64.zip .
#!unzip 64x64.zip
#!ls drive/Practiques_data



 import os
 import glob
 from PIL import Image
 !pip3 install torch torchvision
 import pathlib
 import torch
 import torchvision
 from torch.utils.data import DataLoader
 from torchvision import transforms
 from matplotlib import pyplot as plt
 !pip install panda
 import pandas as pd
 import numpy as np
 from google.colab import files
 transform =  transforms.ToTensor()
 from torch import nn
 import torch.optim as optim

       #GPU
 
 device = torch.device('cuda')     # creates a device object. for cpu ->('cpu')
 #tf.test.gpu_device_name()                  
 #torch.cuda.device_count()
 #torch.cuda.is_available()
 #torch.cuda.get_device_capability(device)



 class Galaxy_Data:
     """Data From F_DR14_ZooSpec_10000_classified."""
     
     def __init__(self):
         FDR14class = pd.read_csv('drive/Practiques_data/F_DR14_ZooSpec_10000_classified.csv')
         self.content = FDR14class.values                         
         
         imgs = self.content[:,2:].reshape(len(FDR14class), 64, 64)       #it's better to transform all the images at once. Less time consuming
         imgs = torch.tensor(imgs, dtype=torch.float)
         imgs = imgs.unsqueeze(1)
        
         labels = self.content[:,1].astype(np.int)
         
         self.imgs = imgs
         self.labels = labels
        
     def __len__(self):
         return len(self.imgs)
    
     def __getitem__(self,i):
         
         # Classification from Galaxy Zoo.  label = 1 -> Spiral; label = 0 -> elliptical
        
         img = self.imgs[i]
         label = self.labels[i]
         return img, label
      
 dr = Galaxy_Data()



 trainset = Galaxy_Data()
 train_loader = DataLoader(trainset, shuffle = True, batch_size = 100)           
 img, label = iter(train_loader).next()

 testset = Galaxy_Data()
 test_loader = DataLoader(testset, shuffle = False, batch_size = 100)



 """Neural Network"""

#PROVAR DIFERENTS LAYERS!!

 class Network(nn.Module):
     def __init__(self):
      
         super(Network, self).__init__()
         
         self.conv1 = nn.Sequential(nn.Conv2d(1,10,5), nn.MaxPool2d(2), nn.ReLU())       
         self.conv2 = nn.Sequential(nn.Conv2d(10,20,5), nn.MaxPool2d(5), nn.ReLU())      
        
         self.linear1 = nn.Sequential(nn.Linear(20*5*5,400), nn.ReLU())
         self.linear2 = nn.Sequential(nn.Linear(400,300), nn.ReLU())
         self.linear3 = nn.Sequential(nn.Linear(300,200), nn.ReLU())
         self.linear4 = nn.Sequential(nn.Linear(200,100), nn.ReLU())               
         self.linear5 = nn.Sequential(nn.Linear(100,50), nn.ReLU())     #Softmax transforms into probabilities. It's already in the definition of CrossEntropyLoss().
         self.linear6 = nn.Sequential(nn.Linear(50,2))                 #therefore, it is not needed here.
        
     def forward(self,x):
 
         x = self.conv1(x)
         x = self.conv2(x)
 
         x = self.linear1(x.view(x.size(0),-1))
         x = self.linear2(x.view(x.size(0),-1))
         x = self.linear3(x.view(x.size(0),-1))
         x = self.linear4(x.view(x.size(0),-1))
         x = self.linear5(x.view(x.size(0),-1))
         x = self.linear6(x.view(x.size(0),-1))
         return x

 net = Network()
 net = net.to(device) 


 """Training part"""

 rg = 100               # THIS VALUE IS OK FOR GPU. 
 loss_function = nn.CrossEntropyLoss()                          
 optimizer = torch.optim.Adam(net.parameters(),lr=1e-3)
 loss_epoch = [] 

 for epoch in range(rg):
  
     #print('====================================\n\n Epoch: %d \n' % epoch)
    
     running_loss = 0.0
     
     for img, label in train_loader:
         img = img.to(device)
         label = label.to(device)
         
         # zero the parameter gradient
         optimizer.zero_grad()
        
         # forward + backward + optimize
         out = net(img)       
         loss = loss_function(out, label)
         loss.backward()
         optimizer.step()
        
         # statistics
         running_loss += loss.item()      
        
     batch_loss = running_loss/len(train_loader)           
     loss_epoch.append(batch_loss)
    
 print('\n======================================\n Finished Training \n')
 loss_epoch


 # 'Test' over the same training sample, without shuffle'.

 L = []

 for img, label in test_loader:
     img = img.to(device)
     out = net(img)
     _, pred = out.max(1)
     pred = pred.detach().cpu().numpy()
     #print(pred, label)
     L.append(np.vstack([pred, label]))
    
 res = pd.DataFrame(np.hstack(L).T, columns = ['pred','label'])
 acc = (res.pred == res.label).mean()
 print(acc)
 #print(res)
