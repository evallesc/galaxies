
import os
import glob
from PIL import Image
import pathlib
import torch
import torchvision
import pandas as pd
import numpy as np
import torch.optim as optim
import tensorflow as tf
from torch.utils.data import DataLoader
from torchvision import transforms
from torch import nn
#from google.colab import files
!pip install torch torchvision
!pip install panda
transform =  transforms.ToTensor()
tf.test.gpu_device_name()



"""DATA FROM DR14_ZooSpec.csv, 100 labeled images"""

root='64x64/'

class Galaxies:
    """Images from SDSS classified by galaxy zoo."""
    
    def __init__(self,root):
        self.transform =  transforms.ToTensor()
        g = os.path.join(root,'*.jpg')
        self.root = root
        self.files = glob.glob(g)
        df = pd.read_csv('drive/Practiques_data/DR14_ZooSpec.csv')
        self.dff = df[['dr7objid','spiral','elliptical','uncertain']]
        
    def __len__(self):
        return len(self.files)
      
    def __getitem__(self, i):
        path = self.files[i] 
        img = Image.open(path)
              
        
        # Classification from Galaxy Zoo.
        objid = int(pathlib.Path(path).stem) 
        dff = self.dff
        tdff = torch.tensor(dff[dff.dr7objid==objid].values[0])
        label = tdff[1:4]
        
        label = (label*torch.tensor([0,1,2])).sum()
        img = self.transform(img)
        
        return img, label
      
ds = Galaxies(root)


trainset = Galaxies(root='64x64')
train_loader = DataLoader(trainset, shuffle = True, batch_size = 1)

img, label = iter(train_loader).next() 


"""Neural Network"""

class Network(nn.Module):
    def __init__(self):
      
        super(Network, self).__init__()
        
        self.conv1 = nn.Sequential(nn.Conv2d(3,6,5), nn.MaxPool2d(2), nn.ReLU())       # [1,3,64,64] -> [1,6,30,30]
        self.conv2 = nn.Sequential(nn.Conv2d(6,10,5), nn.MaxPool2d(5), nn.ReLU())      # [1,6,32,32] -> [1,10,5,5]
        
        self.linear1 = nn.Sequential(nn.Linear(250,100), nn.Softmax())                # [1,10,5,5] -> [1,250] -> [1,100]
        self.linear2 = nn.Sequential(nn.Linear(100,3), nn.Softmax())                  # [1,100] -> [1,3]
        
        
    def forward(self,x):
      
        #print(x.size())
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.linear1(x.view(img.size(0),-1))
        x = self.linear2(x.view(img.size(0),-1))
        #x = self.linear2(x)
        print(x.size)
        return x


net = Network()
#net(img).size()



"""Training part"""

lossfunction = nn.CrossEntropyLoss()                          
optimizer = torch.optim.Adam(net.parameters(),lr=1e-3)

for epoch in range(2):
    
    running_loss = 0.0
    
    for inp, label in train_loader:
        # zero the parameter gradient
        optimizer.zero_grad()
        
        # forward + backward + optimize
        out = net(inp)
        #print(label)
        #print(out.size(), label.size())
        loss = lossfunction(out, label)
        #print(loss)
        loss.backward()
        optimizer.step()
        
        
        # print statistics
        running_loss += loss.item()
        #print('loss: %.3f' % running_loss)
        running_loss = 0.0

print('Finished Training')


