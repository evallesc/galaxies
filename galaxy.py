
root='64x64/'

class Galaxies:
    """Images from SDSS classified by galaxy zoo."""
    
    def __init__(self,root):
        g = os.path.join(root,'*.jpg')
        self.root = root
        self.files = glob.glob(g)
        df = pd.read_csv('DR14_ZooSpec.csv')
        self.dff = df[['dr7objid','spiral','elliptical','uncertain']]
        
    def __len__(self):
        return len(self.files)
      
    def __getitem__(self, i):
        path = self.files[i] 
        img = Image.open(path)
        
        # Classification from Galaxy Zoo.
        objid = int(pathlib.Path(path).stem) 
        dff = self.dff
        tdff = torch.tensor(dff[dff.dr7objid==objid].values[0])
        label = tdff[1:4]
        
        return img, label
      
ds = Galaxies(root)

assert len(ds) == 100
img, label = ds[5]
print(label)

img
